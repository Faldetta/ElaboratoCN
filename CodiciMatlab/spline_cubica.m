function y = spline_cubica(xi, fi, x, tipo)
% y = spline_cubica(xi, fi, x [, tipo])
%   Calcola e valuta i valori della spline cubica naturale o not-a-knot 
% Input:
%   xi = ascisse di interpolazione
%   fi = ordinate di interpolazione
%   x = punti di valutazione della spline 
%   tipo = se inserito, spline naturale  
% Output:
%   y: valutazione dei punti x calcolati sulla spline richiesta
    if nargin <= 3
        y = valore_spline_not_knot(xi, fi, coeff_spline_not_a_knot(xi, fi), x);
    else      
        y = valore_spline_naturale(xi, fi, coeff_spline_naturale(xi, fi), x);
    end
end





