function S = sparsa(n)
% S = sparsa(n)
%   La funzione genera una matrice sparsa
% Input:
%   n = dimensione della matrice;
% Output:
%   S = matrice sparsa
    R = sprand(n, n, 0.3);
    S = zeros(n, n);
    trasp = true;
    for i = -n/2 : n\2 
       if trasp
            S = S + spdiags(R, i, n, n)';
            trasp = ~trasp;
       else
            S = S + spdiags(R, i, n, n);
            trasp = ~trasp;
       end
    end
end