function y = lagrange(xi, fi, x)
% y = lagrange(xi, fi, x)
%   La funzione implementa il calcolo del polinomio interpolante
%   in forma di Lagrange
% Input:
%   xi = vettore delle ascisse di interpolazione
%   fi = vettore dei valori della funzione su x
%   x = vettore dei punti in cui valutare il polinomio
% Output:
%   y = vettore dei valori del polinomio valutato sui punti x
    n = length(xi) - 1;
    m = length(x);
    y=zeros(size(x));
    for i = 1 : m
        for j = 1 : n + 1
            p = 1;
            for k = 1 : n + 1
                if j ~= k
                    p = p * (x(i) - xi(k)) / (xi(j) - xi(k));
                end
            end
            y(i) = y(i) + fi(j) * p;
        end
    end
end