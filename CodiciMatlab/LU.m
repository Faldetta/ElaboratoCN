function A = LU(A)
% A = LU(A)
%   La funzione prende in ingresso una matrice e ne calcola la
%   fattorizzazione LU
% Input:
%   A = matrice da fattorizzare LU
% Output:
%   A = matrice fattorizzata LU 
    n = size(A, 1);
    p = 1 : n;
    for i = 1 : n - 1
        [mi, ki] = max(abs(A(i : n, i)));
        if (mi == 0)
            error("la matrice e' singolare");
        end
        ki = ki + i - 1;
        if (ki > i)
            A([i ki], :) = A([ki i], :);
            p([i ki]) = p([ki i]);
        end
        A(i + 1 : n, i) = A(i + 1 : n, i) / A(i, i);
        A(i + 1 : n, i + 1 : n) = A(i + 1 : n, i + 1 : n) - A(i + 1 : n, i) * A(i, i + 1 : n);
    end
end