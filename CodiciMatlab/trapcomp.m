function If = trapcomp(n, a, b, fun)
% If = trapcomp(n, a, b, fun)
%   La funzione sfrutta la formula composita dei trapezi per il calcolo
%   dell'integrale definito tra a e b della funzione fun 
%   su n+1 ascisse equidistanti tra a e b  
% Input:
% 	n = valore intero positivo
% 	a = estremo sinistro 
% 	b = estremo destro 
% 	fun = funzion integranda
% Output:
% 	If = valutazione dell'integrale di fun tra a e b
	x = linspace( a, b, n+1);
	f = feval( fun, x );
	If = ((b-a)/n)*(f(1)/2 + sum(f(2:n)) + f(n+1)/2);
end
