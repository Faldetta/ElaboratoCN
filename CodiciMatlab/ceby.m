function xi = ceby(n, a, b)
% xi = ceby(n, a, b)
%   La funzione implementa il calcolo delle ascisse di Chebyshev
%   su [a,b]
% Input:
%   n = grado del polinomio interpolante
%   a = estremo sinistro dell'intervallo
%   b = estremo destro dell'intervallo
% Output:
%   xi: ascisse di Chebyshev
    xi = cos((2 * (0 : n) + 1) * pi / (2 * n + 2));
    xi = ((a + b) + (b - a) * xi) / 2;
end
