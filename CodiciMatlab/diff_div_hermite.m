function fi = diff_div_hermite(xi, fi)
% fi = diff_div_hermite(xi, fi)
% La funzione calcola differenze divise per polinomio di Hermite
% Input:
%   xi = vettore delle ascisse
%   fi = vettore dei valori della funzione
% Output:
%   fi = vettore contenente le differenze divise 
    n = length(xi)-1;
    for i = n:-2:3
        fi(i) = (fi(i)-fi(i-2))/(xi(i)-xi(i-1));
    end
    for j = 2:n
        for i = n+1:-1:j+1
            fi(i) = (fi(i)-fi(i-1))/(xi(i)-xi(i-j));
        end
    end
end
