function p = horner_gen(xi, fi, x)
% p = hornerGen(xi, fi, x)
%   La funzione implementa il metodo di horner generalizzato
% Input:
%   fi = vettore dei coefficienti
%   xi = vettore degli zeri del polinomio
%   x = vettore di punti di valutazione del polinomio
% Output;
%   p  = polinomio di newton in x
    n = length(xi) - 1;
    p = fi(n + 1) * ones(size(x));
    for k = 1 : length(x) 
        for i = n : - 1 : 1
            p(k) = p(k) * (x(k) - xi(i)) + fi(i);
        end
    end
end