function If = simpcomp(n, a, b, fun)
% If = simpcomp(n,a,b,fun)
%   La funzione implementa la formula composita di Simpson per il calcolo
%   dell'integrale definito tra a e b della funzione fun
% Input:
% 	n = valore intero positivo pari
% 	a = estremo sinistro 
% 	b = estremo destro
% 	fun = funzione da integrare
% Output:
% 	If = valutazione dell'integrale della funzione tra a e b
	x = linspace(a, b, n + 1);
	f = feval(fun, x);
	If = f(1) + f(n + 1);
	dispari = sum(f(2 : 2 : n));
	pari = sum(f(3 : 2 : n - 1));
	If = (If + 4 * dispari + 2 * pari) * (b - a) / (3 * n);
end
