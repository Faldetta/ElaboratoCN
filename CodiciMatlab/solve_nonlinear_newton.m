function x = solve_nonlinear_newton(F, J, x, tolx, itmax)
% x = solve_nonlinear_newton(F, J, x, tolx, itmax)
%   La funzione risolve il sistema nonlineare F con punto di innesco x 
%   (che in seguito sara' il vettore soluzione) usando
%   la matrice jacobiana J, con tolleranza dell'approssimazione del 
%   risultato tolx, in massimo itmax iterazioni
% Input:
%   F = sistema nonlineare
%   J = matrice jacobiana di F
%   x = punto di innesco
%   tolx = tolleranza dell'approssimazione del risultato
%   itmax = numero massimo di iterazioni
% Output:
%   x = vettore soluzione 
    x0 = 0;
    i = 0;
    while (i < itmax) && (norm(x-x0) >= tolx)
        i = i + 1;
        x0 = x;
        x = x + solve_LU(LU(feval(J, x)), - feval(F, x));
    end
end
        