function x = solve_trinf_diaguni(A, x)
% x = solve_trinf_diaguni(A, x)
%   Il metodo risolve il sistema lineare associato alla matrice A di
%   dimensione n triangolare inferiore a diagonale unitaria e vettore dei
%   termini noti x che in seguito sara' il vettore soluzione.
% Input:
%   A = matrice triangolare inferiore a diagonale unitaria
%   x = vettori dei termini noti
% Output:
%   x = vettore soluzione
    n = size(A, 1);
    for i = 1 : n
        for j = 1 : i-1
            x(i) = x(i) - A(i, j) * x(j);
        end
    end
end