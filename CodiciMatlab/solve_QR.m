function x = solve_QR(A, x)
% x = solve_QR(A, x)
%   La funzione risolve il sistema sovradeterminato associato alla 
%   matrice A (fattorizzata QR)
% Input:
%   A = matrice QR
%   x = vettore dei termini noti
% Output:
%   x = vettore soluzione
    [m, n] = size(A);
    for i = 1 : n
        v = [1; A(i + 1 : m, i)];
        x(i : m) = x(i : m) - ((2 * (v) * v')/(v' * v)) * x(i : m);
    end
    x = solve_trisup_QR(A, x(1 : n));
end