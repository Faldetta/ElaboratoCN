function x = solve_bidiag_inf_diaguni(A, x)
% x = solve_bidiag_inf_diaguni(A, x)
%   La funzione computa la soluzione del sistema lineare associato alla
%   matrice A (bidiagonale inferiore a diagonale unitaria di Toepliz) di 
%   dimensione n e vettore dei termini noti x ,in seguito vettore soluzione.
% Input:
%   A = matrice bidiagonale inferiore a diagonale unitaria di Toepliz
%   x = vettore dei termini noti 
% Output:
%   x = vettore soluzione
    n = size(A,1);
    for i = 2 : n
        j = i -1;
        x(i) = x(i) - A(i, j)*x(j);
        x(i) = x(i) / A(i, i);
    end
end