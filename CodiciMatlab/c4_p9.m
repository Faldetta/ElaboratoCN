table = zeros(20, 2);

for n = 2 : 2 : 40
    table(n/2, 1) = n;
    table(n/2, 2) = (2^(n+1))/(exp(1) * n * log(n));
end