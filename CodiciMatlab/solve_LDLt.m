function x = solve_LDLt(A, x)
% x = solve_LDLt(A, x)
%   La funzione risolve il sistema associato alla matrice A,
%   fattorizzata LDLt dall'algoritmo 3.6, ed al vettore dei termini noti x,
%   in seguito vettore soluzione
% Input:
%   A = matrice LDLt restituita dall'algoritmo 3.6
%   x = vettore dei termini noti
% Output:
%   x = vettore soluzione
    x = solve_trinf_diaguni(A, x);
    x = solve_diag(A, x);
    x = solve_trisup_diaguni(A', x);
end
