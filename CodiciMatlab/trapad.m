function If = trapad(a, b, fun, tol, imax, fa, fb)
% If = trapad(a, b, fun, tol [, imax], fa, fb)
%   La funzione implementa la formula composita dei trapezi adattativa 
%   per il calcolare il valore dell'integrale definito tra a e b 
%   della funzione fun tra a e b con tolleranza tol
% Input:
% 	a = estremo sinistro 
% 	b = estremo destro 
% 	fun = funzione integranda
%	tol = tolleranza 
% 	imax = numero massimo di iterazioni (opzionale)
%   fa = valutazione della funzione in a
%   fb = valutazione della funzione in b
% Output:
% 	If = valutazione dell'integrale tra a e b
	if nargin <= 4
		imax = 100 * max(round(-log(tol)), 10);
	end
	if nargin <= 5
		fa = feval(fun, a);
		fb = feval(fun, b);
	end
	h = b - a;
	xm = (a + b) / 2;
	fm = feval(fun, xm);
	I1 = (h / 2) * (fa + fb); 		   
	If = (h / 2) * (fa / 2 + fm + fb / 2);
	err = abs(If - I1) / 3;
	if err > tol && imax > 0
		If = trapad(a, xm, fun, tol / 2, imax - 1, fa, fm) ...
           + trapad(xm, b, fun, tol / 2, imax - 1, fm, fb);
    end
end
