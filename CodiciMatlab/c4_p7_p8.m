fr = @(x)(1./(1+x.^2));
xr = linspace(-6, 6, 10000);
yr = feval(fr, xr);
table = zeros(20, 3);

for n = 2 : 2 : 40
    table(n/2, 1) = n;
    xi = ceby(n, -6, 6);
    fi = feval(fr, xi);
    yl = lagrange(xi, fi, xr);
    table(n/2, 2) = norm(yr - yl,inf);
    table(n/2, 3) = (2 / pi) * log(n);
end