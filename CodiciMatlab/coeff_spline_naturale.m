function coeff = coeff_spline_naturale(xi, fi)
% coeff = coeff_spline_naturale(xi, fi)
%   La funzione calcola i coefficienti da applicare all'espressione della 
%   spline cubica naturale
% Input:
%   xi = ascisse di interpolazione
%   fi = ordinate di interpolazione
% Output:
%   coeff = coefficienti per calcolare l'espressione della spline cubica
    n = length(xi) - 1;
    phi = zeros(n - 2, 1);
    zi = zeros(n - 2, 1);
    dd = zeros(n - 1, 1);
    hi = xi(2) - xi(1);
    hi1 = xi(3) - xi(2);
    phi(1) = hi / (hi + hi1);
    zi(1) = hi1 / (hi + hi1);
    dd(1) = diff_div(xi(1 : 3), fi(1 : 3));
    for i = 2 : n - 2 
        hi = xi(i) - xi(i - 1);
        hi1 = xi(i + 1) - xi(i);
        phi(i) = hi / (hi + hi1);
        zi(i) = hi1 / (hi + hi1);
        dd(i) = diff_div(xi(i - 1 : i + 1), fi(i - 1 : i + 1));
    end
    dd(i + 1) = diff_div(xi(i : i + 2), fi(i : i + 2));
    u(1) = 2;
    for i = 2 : n - 1 
        l(i) = phi(i - 1) / u(i - 1);
        u(i) = 2 - l(i) * zi(i - 1);
    end
    dd(1) = 6 * dd(1);
    dd(2 : n - 1) = 6 * dd(2 : n - 1) - l(2 : n - 1) * dd(1 : n - 2);
    coeff(n - 1) = dd(n - 1) / u(n - 1);
    coeff(n - 2 : -1 : 1) = (dd(n - 2 : -1 : 1) - zi(n - 2 : -1 : 1)...
        * coeff(n - 1 : -1 : 2)) / u(n - 2 : -1 : 1);
end