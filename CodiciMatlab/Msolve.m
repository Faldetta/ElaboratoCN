function u = Msolve(M, r)
% u = Msolve(M, r)
%   La funzione computa la soluzione del sistema lineare Mx=r
% Input:
%   M = matrice associata al sistema lineare 
%   r = vettore associato al sistema lineare
% Output:
%   u = vettore soluzione
    u = r;
    n = length(u);
    for i = 1 : n
       u(i) = u(i) / M(i, i);
       u(i + 1 : n) = u(i + 1 : n) - M(i + 1 : n, i) * u(i);
    end
end