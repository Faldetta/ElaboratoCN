function y = newton(xi, fi, x)
% y = newton(xi, fi, x)
%   La funzione calcola il polinomio interpolante in forma di Newton
% Input:
%   xi = vettore delle ascisse di interpolazione
%   fi = vettore dei valori della funzione su x
%   x = vettore dei punti in cui valutare il polinomio
% Output:
%   y = vettore dei valori del polinomio valutato sui punti x
    y = horner_gen(xi, diff_div(xi, fi), x);
end
