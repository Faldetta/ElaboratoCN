function A = QR(A)
% A = QR(A)
%   Questo metodo fattorizza la matrice A in una matrice QR
% Input:
%   A = matrice da fattorizzare QR
% Output:
%   A = matrice fattorizzata QR
[m, n] = size(A);
for i = 1: n
    alfa = norm(A(i:m, i));
    if alfa == 0
        error('La matrice non ha rango massimo');
    end
    if A(i, i) >= 0
        alfa = -alfa;
    end
    v = A(i, i) - alfa;
    A(i, i) = alfa;
    A(i + 1 : m, i) = A(i + 1 : m, i) / v;
    beta = - v / alfa;
    A(i : m, i + 1 : n) = A(i : m, i + 1 :n) - (beta * [1; A(i + 1 : m, i)])...
        * ([1 A(i + 1 : m, i)'] * A(i : m, i + 1 :n));
end

    