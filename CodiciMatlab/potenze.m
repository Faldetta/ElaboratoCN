function lambda = potenze(A, tol, x0, maxit)
% lambda = potenze(A, tol, [x0, maxit])
%  La funzione trova l'autovalore dominante della matrice A    
% Input:
%    A = matrice utilizzata per il calcolo
%    tol = tolleranza per l'approssimazione
%    x0 = vettore iniziale
%    maxit = massimo numero di iterazioni
% Output:
%    lambda: matrice sparsa di dimensione n
    n = size(A,1);
    if nargin <= 2
        x = rand(n, 1); 
    else
        x = x0;
    end
    x = x / norm(x);
    if nargin <= 3
        maxit = 100*n*(round(-log(tol),1));
    end
    lambda = inf;
    for i=1:maxit
        lambda0 = lambda;
        v = A * x;
        lambda = x' * v;
        err = abs(lambda - lambda0);
        if err <= tol
            break
        end
        x = v/norm(v);
    end
    if err > tol
        warning("Raggiunto maxit");
    end
end