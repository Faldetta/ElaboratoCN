F = @(x) [2 * x(1) - x(2); 3 * (x(2)^2) - x(1)];
J = @(x)[2,-1; -1,6 * x(2)];
x = [0.5; 0.5];
esatta = [1 / 12; 1 / 6];
itmax = 10^10;
table = zeros(2,4);

for j = 1 : 2
    x = [0.5; 0.5];
    tolx = 10^(-3 * j);
    table(j, 1) = tolx;
    x0 = 0;
    i = 0;
    while (i < itmax) && (norm(x - x0) >= tolx)
        i = i + 1;
        x0 = x;
        x = x + solve_LU(LU(feval(J, x)), - feval(F, x));
    end
    table(j, 2) = i;
    table(j, 3) = norm(x - x0);
    table(j, 4) = norm(esatta - x);
end