tol = 10^(-5);
table = zeros(25, 2);
for n = 100 : 20 : 1000
    table((n - 80) / 20, 1) = n;
    A = sparsa_c6_p1(n);
    D = diag(A);
    b = ones(n, 1);
    x = rand(n,1);
    maxit = 100 * n * round(-log(tol));
    for i = 1 : maxit
        r = A * x - b;
        nr = norm(r, inf);
        if nr <= tol
            table((n - 80) / 20, 2) = i;
            break;
        end
        r = r ./ D;
        x = x - r;   
    end
    if nr > tol
        warning('raggiunto maxit');
        table((n - 80) / 20, 2) = 'raggiunto maxit' + i;
    end
end
    
