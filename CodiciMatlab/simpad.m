function If = simpad(a, b, fun, tol, imax, fa, f1, fb)
% If = simpad(a, b, fun, tol [, imax, fa, f1, fb])
% La funzione implementa la formula composita di Simpson adattativa 
% nell'intervallo [a,b]
% Input:
% 	a = estremo sinistro 
% 	b = estremo destro 
% 	fun = funzion integranda
%	tol = tolleranza
% 	imax = numero massimo di iterazioni (opzionale)
%   fa = valutazione della funzione in a
%   f1 = valutazione della funzione in x1
%   fb = valutazione della funzione in b
% Output:
% 	If = approssimazione dell'integrale definito di fun tra a e b.
	x1 = (a + b) / 2;
	if nargin <= 5
		fa = feval(fun, a);
		fb = feval(fun, b);
		f1 = feval(fun, x1);
	end
	if nargin <= 4
		imax = 4 * max(round(-log(tol)), 10);
	end
	h = b - a;
	I2 = (h / 6) * (fa + 4 * f1 + fb); 	
	f2 = feval(fun, (a + x1) / 2);
	f3 = feval(fun, (x1 + b) / 2);	
	If = h * (fa + 4 * f2 + 2 * f1 + 4 * f3 + fb) / 12;
	err = abs(If - I2) / 15;
	if err > tol && imax > 0
		If = simpad(a, x1, fun, tol / 2, imax - 1, fa, f2, f1)...
            + simpad(x1, b, fun, tol / 2, imax - 1, f1, f3, fb);
    end
end
