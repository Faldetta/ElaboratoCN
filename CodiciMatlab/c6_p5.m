n = 1000;
tol = 10^(-5);

table_j = zeros(1, 2);
A = sparsa_c6_p1(n);
D = diag(A);
b = ones(n, 1);
x = rand(n, 1);
maxit = 100 * n * round(-log(tol));
for i = 1 : maxit
    table_j(i, 1) = i;
    r = A * x - b;
    nr = norm(r, inf);
    table_j(i, 2) = nr;
    if nr <= tol
        break;
    end
    r = r ./ D;
    x = x - r;   
end
if nr > tol
    warning('raggiunto maxit');
end
    
table_gs = zeros(1, 2);
A = sparsa_c6_p1(n);
D = diag(A);
b = ones(n, 1);
x = rand(n,1);
maxit = 100 * n * round(-log(tol));
for i = 1 : maxit
    table_gs(i, 1) = i;
    r = A * x - b;
    nr = norm(r, inf);
    table_gs(i, 2) = nr;
    if nr <= tol
        break;
    end
    r = Msolve(A, r);
    x = x - r;
end
if nr > tol
    warning('raggiunto maxit');
end