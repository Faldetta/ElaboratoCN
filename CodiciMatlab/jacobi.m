function x = jacobi(A, b, tol, x0, maxit)
% x = jacobi(A, b, tol, [xo, maxit])
%   La funzione risolve il sistema lineare Ax=b con il 
%   metodo di Jacobi 
% Input:
%   A = matrice utilizzata per il calcolo
%   b = vettore dei termini noti
%   tol = tolleranza dell' approssimazione
%   x0 = vettore iniziale
%   maxit = numero massimo di iterazioni
% Output:
%   x = soluzione del sistema
    D = diag(A);
    n = length(b);
    if nargin <= 3
        x = rand(n, 1);
    else
        x = x0;
    end
    if nargin <= 4
        maxit = 100 * n * round(-log(tol));
    end
    for i = 1 : maxit
        r = A * x - b;
        nr = norm(r, inf);
        if nr <= tol
            break;
        end
        r = r ./ D;
        x = x - r;   
    end
    if nr > tol
        warning('raggiunto maxit');
    end
end