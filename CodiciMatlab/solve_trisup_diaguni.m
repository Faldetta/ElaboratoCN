function x = solve_trisup_diaguni(A, x)
% x = trisup_diaguni(A, x)
%   La funzione risolve il sistema lineare associato alla matrice A 
%   triangolare superiore a diagonale unitaria e vettore dei
%   termini noti x che in seguito sara' il vettore soluzione.
% Input:
%   A = matrice triangolare superiore a diagonale unitaria
%   x = vettori dei termini noti
% Output:
%   x = vettore soluzione
    n = size(A, 1);
    for i = n : -1 : 1
        for j = i + 1 : n
            x(i) = x(i) - A(i, j) * x(j);
        end
    end
end