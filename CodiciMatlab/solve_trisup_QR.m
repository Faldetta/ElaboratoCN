function x = solve_trisup_QR(A, x)
% x = solve_trisup_QR(A, x)
%   Il metodo risolve il sistema lineare associato alla matrice A di
%   dimensione n triangolare superiorre e vettore dei
%   termini noti x che in seguito sara' il vettore soluzione.
% Input:
%   A = matrice triangolare superiore
%   x = vettori dei termini noti
% Output:
%   x = vettore soluzione
    n = length(x);
    for i = n : -1 : 1
        for j = i + 1 : n
            x(i) = x(i) - A(i, j) * x(j);
        end
        x(i) = x(i) / A(i, i);
    end
end