function x = solve_diag(A, x)
% x = solve_diag(A, x)
%   La funzione risolve il sistema associato alla matrice A , 
%   diagonale, ed al vettore dei termini noti x, che sara' in seguito il
%   vettore soluzione.
% Input:
%   A = matrice diagonale
%   x = vettore dei termini noti
% Output:
%   x = vettore soluzione
    n = size(A, 1);
    for i = 1 : n
        x(i) = x(i) / A(i, i);
    end
end