table = zeros(10, 3);
tol = 10^(-5);
for i=100:100:1000
    table(i/100, 1) = i;
    A = sparsa_c6_p1(i);
    n = size(A,1);
    x = rand(n, 1); 
    x = x / norm(x);
    maxit = 100*n*(round(-log(tol),1));
    lambda = inf;
    for j=1:maxit
        lambda0 = lambda;
        v = A * x;
        lambda = x' * v;
        err = abs(lambda - lambda0);
        if err <= tol
            table(i/100, 3) = lambda;
            break
        end
        x = v/norm(v);
    end
        table(i/100, 2) = j;
    if err > tol
        warning("Raggiunto maxit");
        table(i/100, 3) = "Raggiunto maxit "+lambda;
    end
end