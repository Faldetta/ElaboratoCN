function x = solve_LU(A, x)
% x = solve_LU(A, x)
%   La funzione risolve il sistema associato alla matrice A,
%   fattorizzata LU dall'algoritmo 3.7, ed al vettore dei termini noti x, 
%   in seguito vettore soluzione
% Input;
%   A = matrice LU restituita dall'algoritmo 3.7
%   x = vettore dei termini noti
% Output:
%   x = vettore soluzione
    x = solve_trinf_diaguni(A, x);
    x = solve_trisup(A, x);
end