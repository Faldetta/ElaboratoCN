function y = hermite(xi, fi, fi1, x)
% x = hermite(xi, fi, fi1, x)
%   La funzione calcola il polinomio interpolante di Hermite
% Input:
%   xi = vettore delle ascisse di interpolazione
%   fi = vettore dei valori della funzione su x
%   fi1 = vettore della derivata su x
%   x = vettore dei punti in cui valutare il polinomio
% Output:
%   y = vettore dei valori del polinomio valutato sui punti x.
    xi = reshape([xi; xi], [], 1)';
    fi = reshape([fi; fi1], [], 1)';
    y = horner_gen(xi, diff_div_hermite(xi,  fi) ,x);
end