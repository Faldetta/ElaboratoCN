function S = sparsa_c6_p1(n)
% S = sparsa(n)
%   La funzione genera la matrice sparsa nxn, n>10
% Input:
%   n = dimensione della matrice
% Output:
%   S = matrice sparsa per diagonali
    if(n<=10)
        n=11;
    end
    d = ones(n,1)*4;
    S = spdiags(d,0,n,n);
    d = ones(n,1)*(-1);
    S = spdiags(d,1,S);
    S = spdiags(d,-1,S);
    S = spdiags(d,10,S);
    S = spdiags(d,-10,S);
end